#!/bin/sh

ICONS_DIR=debian/joplin/usr/share/icons/hicolor

for icon in Assets/LinuxIcons/*; do
    sub_dir=$(basename $icon .png)
    mkdir -p $ICONS_DIR/$sub_dir/apps
    cp $icon $ICONS_DIR/$sub_dir/apps/joplin.png
done

mkdir -p $ICONS_DIR/scalable/apps
cp Assets/JoplinIcon.svg $ICONS_DIR/scalable/apps/joplin.svg
